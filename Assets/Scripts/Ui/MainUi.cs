﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ui
{
    public class MainUi : MonoBehaviour
    {
        private void Start() => Time.timeScale = 0;

        public void RestartGame() => 
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        public void StartGame() => 
            Time.timeScale = 1;
    }
}