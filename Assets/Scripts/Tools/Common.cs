﻿using UnityEngine;

namespace Tools
{
    public static class Common
    {
        /// <summary>
        /// Moving sth
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="direction"></param>
        /// <param name="speed"></param>
        public static void Move(Transform transform,float direction, float speed)
        {
            var position = transform.position;
            transform.Translate(new Vector3(direction, position.y, position.z) * speed);
        }
    }
}