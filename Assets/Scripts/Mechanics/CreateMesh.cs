﻿using UnityEngine;

namespace Mechanics
{
    public class CreateMesh : MonoBehaviour
    {
        /// <summary>
        /// Add mesh-cube to current mesh
        /// </summary>
        /// <param name="width"></param>
        /// <param name="length"></param>
        /// <param name="height"></param>
        /// <param name="position"></param>
        /// <param name="curMesh"></param>
        /// <returns></returns>
        public static Mesh AddCube(Vector3 width, Vector3 length, Vector3 height, Vector3 position, Mesh curMesh)
        {
            var corner0 = position + (-width / 2 - length / 2 - height / 2);
            var corner1 = position + (width / 2 + length / 2 + height / 2);

            var combine = new CombineInstance[7];
            combine[0].mesh = Quad(corner0, length, width);
            combine[1].mesh = Quad(corner0, width, height);
            combine[2].mesh = Quad(corner0, height, length);
            combine[3].mesh = Quad(corner1, -width, -length);
            combine[4].mesh = Quad(corner1, -height, -width);
            combine[5].mesh = Quad(corner1, -length, -height);
            combine[6].mesh = curMesh;

            Mesh mesh = new Mesh();
            
            mesh.CombineMeshes(combine, true, false);
            return mesh;
        }

        /// <summary>
        /// Creation Quad
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="width"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private static Mesh Quad(Vector3 origin, Vector3 width, Vector3 length)
        {
            var normal = Vector3.Cross(length, width).normalized;

            var mesh = new Mesh
            {
                vertices = new[] {origin, origin + length, origin + length + width, origin + width},
                normals = new[] {normal, normal, normal, normal},
                uv = new[] {new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0)},
                triangles = new[] {0, 1, 2, 0, 2, 3}
            };
            
            return mesh;
        }
    }
}