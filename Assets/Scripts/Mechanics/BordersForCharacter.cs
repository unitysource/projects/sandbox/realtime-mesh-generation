﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Mechanics
{
    public class BordersForCharacter : MonoBehaviour
    {
        [SerializeField] private float leftWall;
        [SerializeField] private float rightWall;
        [SerializeField] private Transform character;

        /// <summary>
        /// Generete and update borders for character position (x statement)
        /// </summary>
        private void FixedUpdate()
        {
            Vector3 pos = character.position;
            if(pos.x < leftWall)
                character.position = new Vector3(Mathf.Clamp(pos.x, leftWall, rightWall), pos.y, pos.z);
            if (pos.x >= rightWall)
                character.position = Vector3.Lerp(pos, new Vector3(leftWall, pos.y, pos.z), .3f);
        }
    }
}