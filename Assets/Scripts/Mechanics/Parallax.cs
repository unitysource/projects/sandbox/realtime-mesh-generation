﻿using System;
using System.Collections;
using Tools;
using UnityEngine;

namespace Mechanics
{
    public class Parallax : MonoBehaviour
    {
        [SerializeField] private float resetDelay;
        
        private float _startPos;
        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
            _startPos = transform.position.x;
            StartCoroutine(ResetBg());
        }

        /// <summary>
        /// Return BG to start position every delay-time
        /// </summary>
        /// <returns></returns>
        private IEnumerator ResetBg()
        {
            while (_camera)
            {
                yield return new WaitForSeconds(resetDelay);
                var locTr = transform;
                var position = locTr.position;
                position = new Vector3(_startPos, position.y, position.z);
                locTr.position = position;
            }
        }
    }
}