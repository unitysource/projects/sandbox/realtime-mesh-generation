﻿using System.Collections.Generic;
using UnityEngine;

namespace Mechanics
{
    public class DrawLine : MonoBehaviour
    {
        [SerializeField] private DrawMesh drawMesh;

        [Header("Line And Draw Stuff")]
        [SerializeField] private GameObject linePrefab;
        [SerializeField] private GameObject drawField;
        [SerializeField] private Canvas drawCanvas;

        [Tooltip("Min distance between this and before point to create line")] [SerializeField]
        private float minDistance;

        [SerializeField] private int maxPoints = 300;

        private GameObject _curLine;
        private LineRenderer _lineRenderer;
        private List<Vector2> _points;
        private Camera _main;
        private float _planeDistance;
        private Vector3[] _drawFieldCorners;

        private Vector3 _minPositionOfCursor;
        private Vector3 _maxPositionOfCursor;

        private void Start()
        {
            _main = Camera.main;
            _planeDistance = drawCanvas.planeDistance;
            _drawFieldCorners = new Vector3[4];
            drawField.GetComponent<RectTransform>().GetWorldCorners(_drawFieldCorners); // 0-l_b, 1-l_t, 2-r_t, 3-r_b 

            _points = new List<Vector2>();
            _curLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
            _lineRenderer = _curLine.GetComponent<LineRenderer>();
            _lineRenderer.sortingOrder = 4;

            var locPoints = new[] {Vector2.zero};
            drawMesh.GenerateMesh(locPoints);
            drawMesh.FindPositionForPoints(.2f);
            _points.Clear();
        }

        private void Update()
        {
            if (!drawCanvas.isActiveAndEnabled
                || _points.Count >= maxPoints) return;

            if (Input.GetMouseButtonDown(0)) 
                Time.timeScale = 0f;

            if (Input.GetMouseButton(0))
            {
                Vector3 mousePosition = Input.mousePosition + Vector3.forward * _planeDistance;
                Vector3 tempPos = _main.ScreenToWorldPoint(mousePosition);

                if (ConfigBorders(tempPos)) return;

                if (_points.Count == 0)
                {
                    _minPositionOfCursor = _maxPositionOfCursor = tempPos;
                    CreateNewLine(tempPos);
                }

                if (Vector2.Distance(tempPos, _points[_points.Count - 1]) > minDistance)
                    CreateNewLine(tempPos);
            }

            if (Input.GetMouseButtonUp(0))
            {
                Time.timeScale = 1f;
                if (_points.Count <= 1)
                {
                    DestroyAllPoints();
                    return;
                }
                drawMesh.ResetParents();
                ConfigMesh();
                DestroyAllPoints();
            }

            bool ConfigBorders(Vector3 tempPos) =>
                tempPos.x <= _drawFieldCorners[0].x ||
                tempPos.x >= _drawFieldCorners[2].x ||
                tempPos.y <= _drawFieldCorners[0].y ||
                tempPos.y >= _drawFieldCorners[2].y;
        }

        /// <summary>
        /// Generation cube mesh
        /// </summary>
        private void ConfigMesh()
        {
            Vector2 center = (_minPositionOfCursor + _maxPositionOfCursor) * 0.5f;

            for (var i = 0; i < _points.Count; i++)
                _points[i] -= center;

            drawMesh.GenerateMesh(_points);
            drawMesh.FindPositionForPoints(_maxPositionOfCursor.y - _minPositionOfCursor.y);
        }

        /// <summary>
        /// Generate new line betweem current and new point
        /// </summary>
        /// <param name="fingerPos">Point of player click on</param>
        private void CreateNewLine(Vector3 fingerPos)
        {
            float vectorLength = fingerPos.magnitude;
            if (vectorLength > _maxPositionOfCursor.magnitude)
                _maxPositionOfCursor = fingerPos;
            else if (vectorLength < _minPositionOfCursor.magnitude)
                _minPositionOfCursor = fingerPos;

            _points.Add(fingerPos);
            var positionCount = _lineRenderer.positionCount;
            positionCount++;
            _lineRenderer.positionCount = positionCount;
            _lineRenderer.SetPosition(positionCount - 1, fingerPos);
        }

        private void DestroyAllPoints()
        {
            _lineRenderer.positionCount = 0;
            _points.Clear();
        }
    }
}