﻿using UnityEngine;

namespace Mechanics
{
    public class MeshGeneration : MonoBehaviour
    {
        private Camera _camera;

        [SerializeField] private LineRenderer line;
        [SerializeField] private GameObject meshObject;

        private MeshFilter _meshFilter;
        // private MeshCollider _meshCollider;
        [SerializeField] private float scale = 1f;

        private void Start()
        {
            _camera = Camera.main;
            _meshFilter = meshObject.GetComponent<MeshFilter>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 clickPosition = -Vector3.one;

                clickPosition =
                    _camera.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 5f);
                Debug.Log(clickPosition);

                var positionCount = line.positionCount;
                positionCount++;
                line.positionCount = positionCount;
                line.SetPosition(positionCount - 1, clickPosition);

                Mesh newMesh = CreateMesh.AddCube(Vector3.right * scale, Vector3.forward * scale, Vector3.up * scale,
                    clickPosition, _meshFilter.mesh);
                _meshFilter.sharedMesh = newMesh;
                var col = meshObject.AddComponent<MeshCollider>();
                col.convex = true;
            }

            if (Input.GetKey(KeyCode.Space))
            {
                line.positionCount = 1;
                line.SetPosition(line.positionCount - 1, Vector3.forward * -5f);
            }
        }
    }
}