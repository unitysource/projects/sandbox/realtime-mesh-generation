﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Mechanics
{
    public class DrawMesh : MonoBehaviour
    {
        [SerializeField] private GameObject meshObjectPrefab;
        [SerializeField] private float scale = 1f;

        [SerializeField] private Transform[] parents;

        private MeshFilter _meshFilter;
        private MeshCollider _meshCollider;

        private Vector2 _curPoint;
        private float _bigSquare;

        private void Awake() =>
            _bigSquare = scale * 1.7f;

        /// <summary>
        /// Updating current mesh-object
        /// </summary>
        /// <param name="points"></param>
        public void GenerateMesh(IEnumerable<Vector2> points)
        {
            var enumerable = points as Vector2[] ?? points.ToArray();

            foreach (var point in enumerable)
            {
                if (_curPoint == Vector2.zero)
                    _curPoint = point;
                
                // foreach (var parent in parents)
                // foreach (Transform child in parent)
                //     Destroy(child.gameObject);

                var p1 = point;
                var p2 = _curPoint;
                var width = Vector2.Distance(p1, p2);
                Vector2 center = (p1 + p2) * 0.5f;
                float angle = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x) * 180f / Mathf.PI;

                var newMeshObj = Instantiate(meshObjectPrefab, parents[1]);
                Mesh newMesh = new Mesh();
                _meshFilter = newMeshObj.GetComponent<MeshFilter>();
                _meshCollider = newMeshObj.GetComponent<MeshCollider>();

                newMeshObj.transform.localPosition = new Vector3(center.x, center.y, 0);
                newMeshObj.transform.Rotate(Vector3.forward * angle);

                if (point == enumerable[0] || point == enumerable.Last())
                    newMesh = CreateMesh.AddCube(Vector3.right * _bigSquare, Vector3.forward * _bigSquare,
                        Vector3.up * _bigSquare,
                        Vector3.zero, newMesh);
                else
                    newMesh = CreateMesh.AddCube(Vector3.right * width, Vector3.forward * scale, Vector3.up * scale,
                        Vector3.zero, newMesh);

                var newSphereTransform = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
                newSphereTransform.localPosition = p1;
                newSphereTransform.localScale = Vector3.one * (scale * 1.4f);
                newSphereTransform.SetParent(parents[1], false);

                _meshFilter.mesh = _meshCollider.sharedMesh = newMesh;
                _curPoint = point;
                
                Instantiate(newMeshObj, parents[0]);
                Instantiate(newSphereTransform.gameObject, parents[0]);
            }

            _curPoint = Vector2.zero;
        }

        /// <summary>
        /// Set the positions for meshes after drawing
        /// </summary>
        /// <param name="height"></param>
        public void FindPositionForPoints(float height)
        {
            foreach (var parent in parents)
                parent.transform.localPosition = Vector3.down * (height * 0.5f);
        }

        public void ResetParents()
        {
            Debug.Log($"parents[0]: {parents[0].childCount}, parents[1]: {parents[1].childCount}");

            // foreach (var parent in parents)
            // foreach (Transform child in parent)
            //     Destroy(child.gameObject);

            for (int i = 0; i < parents.Length; i++)
            {
                for (int j = 0; j < parents[i].childCount; j++)
                {
                    Destroy(parents[i].GetChild(j).gameObject, 0.01f);
                }
            }
            
            Debug.Log($"0: {parents[0].childCount}, 1: {parents[1].childCount}");
        }
    }
}