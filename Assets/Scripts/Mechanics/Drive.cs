﻿using Tools;
using UnityEngine;

namespace Mechanics
{
    public class Drive : MonoBehaviour
    {
        [SerializeField] private float direction = -1f;
        [SerializeField] private float speed = 0.5f;
        
        private void FixedUpdate()
        {
            Common.Move(transform, direction, speed);
        }
    }
}