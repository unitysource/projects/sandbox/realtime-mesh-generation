﻿using UnityEngine;

namespace Mechanics
{
    public class TestMesh : MonoBehaviour
    {
        [SerializeField] private GameObject meshObjects;
        [SerializeField] private Transform[] points;
        [SerializeField] private float scale = 1f;
        private MeshFilter _meshFilters;
        private MeshCollider _meshColliders;

        private void Start()
        {
            _meshFilters = meshObjects.GetComponent<MeshFilter>();
            _meshColliders = meshObjects.GetComponent<MeshCollider>();

            _meshFilters.mesh = new Mesh();
            _meshColliders.sharedMesh = new Mesh();

            var p1 = points[0].position;
            var p2 = points[1].position;
            var width = Vector2.Distance(points[0].position, points[1].position);
            Vector2 center = (points[0].position + points[1].position) * 0.5f;
            float angle = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x) * 180f / Mathf.PI;

            Mesh newMesh = CreateMesh.AddCube(Vector3.right * width, Vector3.forward * scale, Vector3.up * scale,
                center, _meshFilters.mesh);
            _meshFilters.mesh = newMesh;
            
            meshObjects.transform.Rotate(Vector3.forward*angle);
        }
    }
}