# GenerateMeshRunner (Hyper-Casual Game)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)
- [License](#license)

## About the Game

Sandbox for generate mesh runner game. The game is made using Unity and C#.

## Gameplay

Draw mesh and use it to run.

![Gameplay GIF](/images/gameplay.gif)

## Features

Key features of game:

Realtime mesh generation.

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/sandbox/realtime-mesh-generation.git`
2. Navigate to the game directory: `cd realtime-mesh-generation`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Draw mesh: `Touch and drag`

## Acknowledgments

